﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Start is called before the first frame update

    // Camera and hole should start to move together and stop together to achive this velocities should be calculated for given constant time
    public static float  travelTime = 2.5f;
    public Transform SecondStageTransform;
    private bool isTravelling=false;
    Rigidbody m_rb;
    Animator ShakeAnimation;
    void Start(){
        ShakeAnimation= GetComponent<Animator>();
        m_rb=GetComponent<Rigidbody>();
    }
    public void go2SecondStagePosition(){
        Vector3 velocity = (SecondStageTransform.position-transform.position)/travelTime;
        m_rb.velocity= velocity;
        isTravelling=true;
    }
    void Update(){
        if(isTravelling){
            Vector3 destination =SecondStageTransform.position-transform.position;
            if(destination.z<0){
                m_rb.velocity=Vector3.zero;
                isTravelling=false;
                
            }
        }
    }
    public void shakeCamera(){
        ShakeAnimation.Play("CameraShake",0);
    }
}
