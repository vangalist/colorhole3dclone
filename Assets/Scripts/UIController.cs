﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject gameOverScreen;
    public GameObject levelCompletedScreen;
    public GameObject swipeToStartScreen;
    public ProgressController progressController;

    public void setActiveGameOverScreen(bool active)
    {
        gameOverScreen.SetActive(active);
    }
    public void setActiveLevelCompletedScreen(bool active)
    {
        levelCompletedScreen.SetActive(active);
    }
    public void setActiveSwipeToStartScreen(bool active)
    {
       if(active){
           swipeToStartScreen.SetActive(true);
       }else{
           swipeToStartScreen.GetComponent<Animator>().Play("StartToStartScreenFadeOut",0 );
           //swipeToStartScreen.GetComponent<Animation>().Play();
       }

    }

    public void setProgress(int stage , float progress){
        progressController.setProgress(stage,progress);
    }
    public void setLevelTexts(int currentLevel){
        progressController.setLevelTexts(currentLevel);
    }


}
