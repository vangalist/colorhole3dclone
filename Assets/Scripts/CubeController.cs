﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour
{

    public static int DestroyedCubeNumber = 0;
    private float gravityScale = 30f;
    private float holeDepth = -15f;
    public int stage = 1;

    public bool isEnemy = false;

    public static float globalGravity = -9.81f;

    Rigidbody m_rb;
    void Start() {
    isEnemy=gameObject.tag == "EnemyCube";
   } 
    void OnEnable()
    {
        m_rb = GetComponent<Rigidbody>();
        m_rb.useGravity = false;
    }

    void FixedUpdate()
    {
        if (isEnemy)
        {
            if (GameController.isGameActive)
            {
                Vector3 gravity = globalGravity * gravityScale * Vector3.up;
                m_rb.AddForce(gravity, ForceMode.Acceleration);
                destroyEngulfedCube();
            }
        }
        else
        {
            Vector3 gravity = globalGravity * gravityScale * Vector3.up;
            m_rb.AddForce(gravity, ForceMode.Acceleration);
            destroyEngulfedCube();
        }
    }
    void destroyEngulfedCube()
    {

        if (transform.position.y < holeDepth)
        {
            VibrationController.vibratePhone();
            if (gameObject.tag == "Stage1Cube" || gameObject.tag == "Stage2Cube")
            {
                DestroyedCubeNumber += 1;

                Destroy(gameObject);
            }
            else if (gameObject.tag == "EnemyCube")
            {
                GameController.enemyCubeDestroyed();
                
            }
        }
    }
}
