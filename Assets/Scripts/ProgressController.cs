﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressController : MonoBehaviour
{

    //Progress UI controller
    public Text currentLevelText;
    public Text nextLevelText;

    public Scrollbar stage1ScrollBar;

    public Scrollbar stage2ScrollBar;
    public GameObject Stage1Handle;
    public GameObject Stage2Handle;
    public void setProgress(int stage, float progress)
    {
        if (stage == 1)
        {
            stage1ScrollBar.size = progress;
            if (!Stage1Handle.activeSelf && progress>0)
            {
                Stage1Handle.SetActive(true);
            }
        }
        else
        {
            stage2ScrollBar.size = progress;
            if (!Stage2Handle.activeSelf && progress>0)
            {
                Stage2Handle.SetActive(true);
            }
        }
    }

    public void setLevelTexts(int currentLevel)
    {
        currentLevelText.text = (currentLevel + 1).ToString();
        nextLevelText.text = (currentLevel + 2).ToString();
    }
}
