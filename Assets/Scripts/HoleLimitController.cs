﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleLimitController : MonoBehaviour
{
    public Transform stage1UpRight;
    public Transform stage1DownLeft;
    public Transform stage2UpRight;
    public Transform stage2DownLeft;

    public Vector3 properPosition(int stage, Vector3 desiredPosition)
    {
        Vector3 upRightVector;
        Vector3 downLeftVector;
        if (stage == 1)
        {
            upRightVector = stage1UpRight.position;
            downLeftVector = stage1DownLeft.position;
        }
        else
        {
            upRightVector = stage2UpRight.position;
            downLeftVector = stage2DownLeft.position;
        }

        if (desiredPosition.x > upRightVector.x)
        {
            desiredPosition.x = upRightVector.x;
        }
        if (desiredPosition.z > upRightVector.z)
        {
            desiredPosition.z = upRightVector.z;
        }
        if (desiredPosition.x < downLeftVector.x)
        {
            desiredPosition.x = downLeftVector.x;
        }
        if (desiredPosition.z < downLeftVector.z)
        {
            desiredPosition.z = downLeftVector.z;
        }

        return desiredPosition;

    }
}
