﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitonContoller : MonoBehaviour
{
    // Gravion is the source of gravity. when graviton is close to a particle that particle gains gravity and Cube Controller. 

    void OnTriggerEnter(Collider col)
    {


        if (col.tag == "EnemyCube")
        {
            if (GameController.isGameActive)
            {

                Rigidbody rigidbody = col.gameObject.AddComponent<Rigidbody>();

                col.gameObject.GetComponent<CubeController>().enabled = true;
            }

        }
        else
        {

            try
            {
                Rigidbody rigidbody = col.gameObject.AddComponent<Rigidbody>();

                col.gameObject.GetComponent<CubeController>().enabled = true;
            }
            catch
            {

            }
        }
    }
}
