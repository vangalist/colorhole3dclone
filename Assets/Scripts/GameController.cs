﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    public CameraController cameraController;
    public HoleController holeController;
    public DoorController doorController;
    public UIController uiController;
    public static int currentStageNumber = 1;
    public int currenLevel = 1;
    public int numberOfCubesAtThisStage = 0;
    public bool isLevelCompleted = false;
    private static bool isLevelFailed = false;
    public static bool isGameActive = true;
    private bool isGameOver = false;
    public GameObject[] Levels;
    void Start()
    {
        currenLevel = PlayerPrefs.GetInt("currentLevel");
        int levelPrefabIndex = currenLevel % Levels.Length;
        startLevel(levelPrefabIndex);

    }
    void startLevel(int level)
    {
        currentStageNumber = 1;
        Instantiate(Levels[level], gameObject.transform);
        numberOfCubesAtThisStage = GameObject.FindGameObjectsWithTag("Stage1Cube").Length;
        uiController.setLevelTexts(currenLevel);
        uiController.setActiveSwipeToStartScreen(true);
        isGameActive = true;
    }

    void Update()
    {
        if (isLevelCompleted)
        {
            if (Input.GetMouseButtonDown(0))
            {
                PlayerPrefs.SetInt("currentLevel", (currenLevel + 1)%Levels.Length);
                restartGame();
            }
        }
        else if (numberOfCubesAtThisStage == CubeController.DestroyedCubeNumber)
        {
            CubeController.DestroyedCubeNumber = 0;
            activateNextStage();

        }
        else if (isLevelFailed)
        {
            if (!isGameOver)
            {
                activateGameOver();
            }
            else if (Input.GetMouseButtonDown(0))
            {
                restartGame();
            }
        }
        else
        {
            uiController.setProgress(currentStageNumber, (float)CubeController.DestroyedCubeNumber / numberOfCubesAtThisStage);
        }
    }
    void restartGame()
    {
        isLevelFailed = false;
        uiController.setActiveGameOverScreen(false);
        uiController.setActiveLevelCompletedScreen(false);
        CubeController.DestroyedCubeNumber = 0;
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
    void activateGameOver()
    {
        isGameOver = true;
        uiController.setActiveGameOverScreen(true);
        cameraController.shakeCamera();

    }
    void activateSecondStage()
    {
        holeController.go2CenterPosition();
        doorController.goDown();
    }
    public void deactivateActiveEnemyCubes()
    {
        GameObject[] EnemyCubes = GameObject.FindGameObjectsWithTag("EnemyCube");

        foreach (GameObject enemyCube in EnemyCubes)
        {
            deactivateEnemyCube(enemyCube);
        }
    }
    public void deactivateEnemyCube(GameObject enemyCube)
    {
        try
        {

            enemyCube.GetComponent<CubeController>().enabled=false;
            Destroy(enemyCube.GetComponent<Rigidbody>());

            }
        catch
        {

        }
    }
    public void holeCentered()
    {
        cameraController.go2SecondStagePosition();
        holeController.go2SecondStagePosition();
    }
    public void activateNextStage()
    {
        isGameActive = false;
        
        deactivateActiveEnemyCubes();
        if (currentStageNumber == 1)
        {
            activateSecondStage();
            currentStageNumber += 1;
        }
        else
        {
            levelCompleted();
        }
    }
    public void levelCompleted()
    {
        isLevelCompleted = true;
        currentStageNumber = 1;
        uiController.setActiveLevelCompletedScreen(true);
    }
    public void startSecondStage()
    {
        isGameActive = true;
        numberOfCubesAtThisStage = GameObject.FindGameObjectsWithTag("Stage2Cube").Length;
        CubeController.DestroyedCubeNumber = 0;
    }
    public static void enemyCubeDestroyed()
    {
        isLevelFailed = true;
    }
}
