﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeArrayCreator : MonoBehaviour
{
    public GameObject Cube;

    public int x_num;
    public int y_num;
    public int z_num;

    public float spacing;
    
    void Start()
    {
        Vector3 orginVector = transform.position;

        for (int x_counter = 0; x_counter < x_num; x_counter++)
        {
            for (int y_counter = 0; y_counter < y_num; y_counter++)
            {
                for (int z_counter = 0; z_counter < z_num; z_counter++)
                {
                    Vector3 newCubesCordinates = orginVector + new Vector3(x_counter*spacing,y_counter*spacing,z_counter*spacing);
                    Instantiate(Cube,newCubesCordinates,Quaternion.identity);
                }
            }
        }
    }


}
