﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleController : MonoBehaviour
{
    // Start is called before the first frame update

    //since its kinematic velocity cannot be given via rigid body so it is done manually
    public Transform SecondStageTransform;
    public GameController gameController;
    public UIController uiController;
    public HoleLimitController holeLimitController;

    Vector3 beginTouch;
    Vector3 endTouch;
    private float dragCoefficent = 50f;
    private bool isTravelling2SecondStage = false;
    private bool isTravelling2Center = false;
    private Vector3 velocity;
    private Vector3 centerVelocity;
    public float centerVelocityCeofficent=15f;
    private bool isFirstTouchDone=false;
    void Update()
    {
        if (isTravelling2SecondStage)
        {
            Vector3 destination = SecondStageTransform.position - transform.position;
            if (destination.z < 0)
            {
                isTravelling2SecondStage = false;
                gameController.startSecondStage();
            }
        }
        else if (isTravelling2Center)
        {
            float destination = SecondStageTransform.position.x - transform.position.x;
            if (Mathf.Abs(destination) < 0.4f)
            {
                isTravelling2Center = false;
                gameController.holeCentered();
            }
        }
        else
        {

            if (canGetInput())
            {
                GetInputAndAct();
            }
        }

    }
    void FixedUpdate()
    {
        if (isTravelling2SecondStage)
        {
            transform.position = transform.position + velocity * Time.deltaTime;
        }
        else if (isTravelling2Center)
        {
            transform.position = transform.position + centerVelocity * Time.deltaTime;
        }
    }
    bool canGetInput()
    {
        return GameController.isGameActive;
    }
    void GetInputAndAct()
    {

        if (Input.GetMouseButtonDown(0))
        {
            //Touch Started
            beginTouch = Input.mousePosition;
            if(!isFirstTouchDone){
                isFirstTouchDone=true;
                uiController.setActiveSwipeToStartScreen(false);
            }
        }
        else if (Input.GetMouseButton(0))
        {
            //Touch is dragging
            endTouch = Input.mousePosition;
            Vector3 ofset = endTouch - beginTouch;
            dragHole(convertScreenOfset2WorldOfset(ofset));
            beginTouch = endTouch;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            // Touch is ended
        }
    }
    void dragHole(Vector3 drag)
    {
        Vector3 desiredPosition=transform.transform.position + dragCoefficent * drag;
        transform.transform.position = holeLimitController.properPosition(GameController.currentStageNumber,desiredPosition);
    }

    Vector3 convertScreenOfset2WorldOfset(Vector3 screenOfset)
    {
        // to normalize ofsets of touches ofset vector is divided to screen Width so it will work properly on wifferent screen sizes
        Vector3 worldOfset = (new Vector3(screenOfset.x, screenOfset.z, screenOfset.y)) / Screen.width;
        return worldOfset;
    }
    public void go2SecondStagePosition()
    {
    
        velocity = (SecondStageTransform.position - transform.position) / CameraController.travelTime;
        isTravelling2SecondStage = true;
    }
    public void go2CenterPosition()
    {
        float destination = SecondStageTransform.position.x - transform.position.x;
        if(destination>0){
            centerVelocity=centerVelocityCeofficent*Vector3.right;
        }else{
            centerVelocity=centerVelocityCeofficent*Vector3.left;
        }
        isTravelling2Center = true;
    }

}
