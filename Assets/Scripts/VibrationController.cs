﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VibrationController
{
    private static float lastVibrationTime = 0;
    public static float vibrationInterval = 1f;

    public static void vibratePhone()
    {
        float currentTime = Time.time;
        if (currentTime - lastVibrationTime > vibrationInterval)
        {
            // Soft vibration plugin was not free :/ 
           // Handheld.Vibrate();
            Debug.Log("vibrate");
            lastVibrationTime = currentTime;
        }
    }

}
