﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public float closingSpeed=3f;
    private bool isTravelling = false;
    Rigidbody m_rb;
    void Start()
    {
        m_rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        if(isTravelling){
            if(transform.position.y<-5f){
                Destroy(gameObject);
            }
        }
    }
    public void goDown(){
        m_rb.velocity=Vector3.down*closingSpeed;
        isTravelling=true;
    }
}
